if not exist build mkdir build
cd build
cmake -S ../ -B . -G "MinGW Makefiles"
mingw32-make.exe && mingw32-make.exe Shaders
if not exist models mkdir models
Xcopy /E /I ..\HelloVulkan\models models\.
if not exist shaders mkdir shaders
copy ..\HelloVulkan\shaders\*.spv shaders\.
HelloVulkan.exe
cd ..
