//
//  HelloVulkanNonIndexedModel.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 08/11/22.
//

#ifndef HelloVulkanNonIndexedModel_hpp
#define HelloVulkanNonIndexedModel_hpp

#include "HelloVulkanModel.hpp"

class HelloVulkanNonIndexedModel : public HelloVulkanModel {
public:
    HelloVulkanNonIndexedModel(HelloVulkanDevice &device,
                               const std::vector<HelloVulkanVertex> &vertices);
    ~HelloVulkanNonIndexedModel();
    
    void bind(VkCommandBuffer commandBuffer);
    void draw(VkCommandBuffer commandBuffer);
    
    static std::unique_ptr<HelloVulkanNonIndexedModel> createModelFromFile(HelloVulkanDevice &device, const std::string &filepath);

protected:
    void createVertexBuffers(const std::vector<HelloVulkanVertex> &vertices);
};

#endif /* HelloVulkanNonIndexedModel_hpp */
