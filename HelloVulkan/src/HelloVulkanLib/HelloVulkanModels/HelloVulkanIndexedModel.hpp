//
//  HelloVulkanIndexedModel.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 08/11/22.
//

#ifndef HelloVulkanIndexedModel_hpp
#define HelloVulkanIndexedModel_hpp

#include "HelloVulkanModel.hpp"

class HelloVulkanIndexedModel : public HelloVulkanModel {
public:
    HelloVulkanIndexedModel(HelloVulkanDevice &device,
                            const std::vector<HelloVulkanVertex> &vertices,
                            const std::vector<uint32_t> &indices);
    ~HelloVulkanIndexedModel();
    
    void bind(VkCommandBuffer commandBuffer);
    void draw(VkCommandBuffer commandBuffer);
    
    static std::unique_ptr<HelloVulkanIndexedModel> createModelFromFile(HelloVulkanDevice &device, const std::string &filepath);

private:
    void createVertexBuffers(const std::vector<HelloVulkanVertex> &vertices);
    void createIndexBuffers(const std::vector<uint32_t> &indices);

    std::unique_ptr<HelloVulkanBuffer> indexBuffer;
    uint32_t indexCount;
    
};

#endif /* HelloVulkanIndexedModel_hpp */
