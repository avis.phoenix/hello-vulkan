//
//  HelloVulkanModel.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 01/11/22.
//

#ifndef HelloVulkanModel_hpp
#define HelloVulkanModel_hpp

// libs
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

#include <vulkan/vulkan.h>
#include <memory>

#include "HelloVulkanDevice.hpp"
#include "HelloVulkanVertex.hpp"
#include "HelloVulkanModelBuilder.hpp"
#include "HelloVulkanBuffer.hpp"

class HelloVulkanModel{
public:
    HelloVulkanModel(HelloVulkanDevice &device) : vulkanDevice{device}{};
    virtual ~HelloVulkanModel(){};
    
    virtual void bind(VkCommandBuffer commandBuffer) = 0;
    virtual void draw(VkCommandBuffer commandBuffer) = 0;

protected:
    
    HelloVulkanDevice &vulkanDevice;
    std::unique_ptr<HelloVulkanBuffer> vertexBuffer;
    uint32_t vertexCount;
};

#endif /* HelloVulkanModel_hpp */
