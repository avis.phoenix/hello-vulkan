//
//  HelloVulkanModelBuilder.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 08/11/22.
//

#ifndef HelloVulkanModelBuilder_hpp
#define HelloVulkanModelBuilder_hpp

#include "HelloVulkanVertex.hpp"
#include <string>

class HelloVulkanModelBuilder{
public:
    std::vector<HelloVulkanVertex> vertices{};
    std::vector<uint32_t> indices{};
    
    void loadObjModel(const std::string &filepath);
private:
    std::string getVertexID(HelloVulkanVertex &vertex);
};

#endif /* HelloVulkanModelBuilder_hpp */
