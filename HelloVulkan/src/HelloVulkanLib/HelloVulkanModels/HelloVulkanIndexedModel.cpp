//
//  HelloVulkanIndexedModel.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 08/11/22.
//

#include "HelloVulkanIndexedModel.hpp"

HelloVulkanIndexedModel::HelloVulkanIndexedModel(HelloVulkanDevice &device,
                                                 const std::vector<HelloVulkanVertex> &vertices,
                                                 const std::vector<uint32_t> &indices)
:HelloVulkanModel(device){
    createVertexBuffers(vertices);
    createIndexBuffers(indices);
}

HelloVulkanIndexedModel::~HelloVulkanIndexedModel(){
}

void HelloVulkanIndexedModel::createVertexBuffers(const std::vector<HelloVulkanVertex> &vertices){
    vertexCount = static_cast<uint32_t>(vertices.size());
    assert(vertexCount >= 3 && "Vertex count must be at least 3");
    uint32_t vertexSize = sizeof(vertices[0]);
    VkDeviceSize bufferSize = vertexSize * vertexCount;
    
    
    HelloVulkanBuffer stagingBuffer{
        vulkanDevice,
        vertexSize,
        vertexCount,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    };
    
    stagingBuffer.map();
    stagingBuffer.writeToBuffer((void *)vertices.data());
    stagingBuffer.unmap();
    
    vertexBuffer = std::make_unique<HelloVulkanBuffer>(vulkanDevice,
                                                       vertexSize,
                                                       vertexCount,
                                                       VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                                                       VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    
    vulkanDevice.copyBuffer(stagingBuffer.getBuffer(), vertexBuffer->getBuffer(), bufferSize);
}

void HelloVulkanIndexedModel::createIndexBuffers(const std::vector<uint32_t> &indices){
    indexCount = static_cast<uint32_t>(indices.size());
    assert(indexCount >= 1 && "Index count must be at least 1");
    
    VkDeviceSize bufferSize = sizeof(indices[0]) * indexCount;
    uint32_t indexSize = sizeof(indices[0]);
    
    HelloVulkanBuffer stagingBuffer{
        vulkanDevice,
        indexSize,
        indexCount,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    };
    
    stagingBuffer.map();
    stagingBuffer.writeToBuffer((void *)indices.data());
    
    indexBuffer = std::make_unique<HelloVulkanBuffer>(vulkanDevice,
                                                      indexSize,
                                                      indexCount,
                                                      VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                                                      VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    
    vulkanDevice.copyBuffer(stagingBuffer.getBuffer(), indexBuffer->getBuffer(), bufferSize);
}

void HelloVulkanIndexedModel::bind(VkCommandBuffer commandBuffer){
    VkBuffer buffers[] = {vertexBuffer->getBuffer()};
    VkDeviceSize offsets[] = {0};
    
    vkCmdBindVertexBuffers(commandBuffer, 0, 1, buffers, offsets);
    vkCmdBindIndexBuffer(commandBuffer, indexBuffer->getBuffer(), 0, VK_INDEX_TYPE_UINT32);
}

void HelloVulkanIndexedModel::draw(VkCommandBuffer commandBuffer){
    vkCmdDrawIndexed(commandBuffer, indexCount, 1, 0, 0, 0);
}

std::unique_ptr<HelloVulkanIndexedModel> HelloVulkanIndexedModel::createModelFromFile(HelloVulkanDevice &device,
                                                                                      const std::string &filepath) {
  HelloVulkanModelBuilder builder{};
  builder.loadObjModel(filepath);
  return std::make_unique<HelloVulkanIndexedModel>(device, builder.vertices, builder.indices );
}
