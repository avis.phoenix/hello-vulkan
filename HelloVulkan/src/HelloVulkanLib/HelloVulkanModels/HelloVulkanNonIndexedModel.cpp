//
//  HelloVulkanNonIndexedModel.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 08/11/22.
//

#include "HelloVulkanNonIndexedModel.hpp"
#include "HelloVulkanModelBuilder.hpp"

HelloVulkanNonIndexedModel::HelloVulkanNonIndexedModel(HelloVulkanDevice &device,
                                                       const std::vector<HelloVulkanVertex> &vertices)
: HelloVulkanModel(device){
    createVertexBuffers(vertices);
}

HelloVulkanNonIndexedModel::~HelloVulkanNonIndexedModel() {
}

void HelloVulkanNonIndexedModel::createVertexBuffers(const std::vector<HelloVulkanVertex> &vertices) {
    vertexCount = static_cast<uint32_t>(vertices.size());
    assert(vertexCount >= 3 && "Vertex count must be at least 3");
    uint32_t vertexSize = sizeof(vertices[0]);
    
    HelloVulkanBuffer vertexBufferMemory{
        vulkanDevice,
        vertexSize,
        vertexCount,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    };
    
    vertexBufferMemory.map();
    vertexBufferMemory.writeToBuffer((void *)vertices.data());
    vertexBufferMemory.unmap();
}

void HelloVulkanNonIndexedModel::draw(VkCommandBuffer commandBuffer) {
    vkCmdDraw(commandBuffer, vertexCount, 1, 0, 0);
}

void HelloVulkanNonIndexedModel::bind(VkCommandBuffer commandBuffer) {
    VkBuffer buffers[] = {vertexBuffer->getBuffer()};
    VkDeviceSize offsets[] = {0};
    vkCmdBindVertexBuffers(commandBuffer, 0, 1, buffers, offsets);
}

std::unique_ptr<HelloVulkanNonIndexedModel> HelloVulkanNonIndexedModel::createModelFromFile(HelloVulkanDevice &device,
                                                                                            const std::string &filepath) {
  HelloVulkanModelBuilder builder{};
  builder.loadObjModel(filepath);
  return std::make_unique<HelloVulkanNonIndexedModel>(device, builder.vertices);
}
