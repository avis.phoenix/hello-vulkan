//
//  HelloVulkanVertex.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 01/11/22.
//

#ifndef HelloVulkanVertex_hpp
#define HelloVulkanVertex_hpp

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>

#include <vulkan/vulkan.h>
#include <vector>

class HelloVulkanVertex {
public:
    glm::vec3 position;
    glm::vec3 color;
    glm::vec3 normal{};
    glm::vec2 uv{};
    
    HelloVulkanVertex(){};

    static std::vector<VkVertexInputBindingDescription> getBindingDescriptions();
    static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();
    
    bool operator==(const HelloVulkanVertex &other) const {
        return position == other.position && color == other.color && normal == other.normal &&
        uv == other.uv;
    }
};

// from: https://stackoverflow.com/a/57595105
template <typename T, typename... Rest>
void hashCombine(std::size_t& seed, const T& v, const Rest&... rest) {
  seed ^= std::hash<T>{}(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  (hashCombine(seed, rest), ...);
};

namespace std {
template <>
struct hash<HelloVulkanVertex> {
  size_t operator()(HelloVulkanVertex const &vertex) const {
    size_t seed = 0;
    hashCombine(seed, vertex.position, vertex.color, vertex.normal, vertex.uv);
    return seed;
  }
};
}  // namespace std

#endif /* HelloVulkanVertex_hpp */
