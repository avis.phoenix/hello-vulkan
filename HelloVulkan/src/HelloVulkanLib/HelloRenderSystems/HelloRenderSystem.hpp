//
//  HelloRenderSystem.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 04/11/22.
//

#ifndef HelloRenderSystem_hpp
#define HelloRenderSystem_hpp

#include <vulkan/vulkan.h>

// std
#include <memory>
#include <vector>

#include "HelloVulkanDevice.hpp"
#include "HelloVulkanPipeline.hpp"
#include "HelloVulkanGameObject.hpp"
#include "HelloVulkanFrameInfo.hpp"

#include "HelloVulkanDefaultPaths.h"

struct SimplePushConstantData {
    glm::mat4 modelMatrix{1.f};
    glm::mat4 normalMatrix{1.f};
};

class HelloRenderSystem {
public:
    HelloRenderSystem(HelloVulkanDevice &device,
                      VkRenderPass renderPass,
                      VkDescriptorSetLayout globalSetLayout);
    ~HelloRenderSystem();
    
    // Not copyable or movable
    HelloRenderSystem(const HelloRenderSystem &) = delete;
    HelloRenderSystem &operator=(const HelloRenderSystem &) = delete;
    HelloRenderSystem(HelloRenderSystem &&) = delete;
    HelloRenderSystem &operator=(HelloRenderSystem &&) = delete;
    
    void renderGameObjects(HelloVulkanFrameInfo &frameInfo);
    
private:
    void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
    void createPipeline(VkRenderPass renderPass);
    
    HelloVulkanDevice &vulkanDevice;
    
    std::unique_ptr<HelloVulkanPipeline> vulkanPipeline;
    VkPipelineLayout pipelineLayout;
    
    
    const std::string shadersPath = SHADERS_DIR;
};

#endif /* HelloRenderSystem_hpp */
