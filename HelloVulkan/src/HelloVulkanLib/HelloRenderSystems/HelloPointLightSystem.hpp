//
//  HelloPointLightSystem.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 19/11/22.
//

#ifndef HelloPointLightSystem_hpp
#define HelloPointLightSystem_hpp

#include <vulkan/vulkan.h>

// std
#include <memory>
#include <vector>

#include "HelloVulkanDevice.hpp"
#include "HelloVulkanPipeline.hpp"
#include "HelloVulkanGameObject.hpp"
#include "HelloVulkanFrameInfo.hpp"

#include "HelloVulkanDefaultPaths.h"

class HelloPointLightSystem {
public:
    HelloPointLightSystem(HelloVulkanDevice &device,
                          VkRenderPass renderPass,
                          VkDescriptorSetLayout globalSetLayout);
    ~HelloPointLightSystem();
    
    // Not copyable or movable
    HelloPointLightSystem(const HelloPointLightSystem &) = delete;
    HelloPointLightSystem &operator=(const HelloPointLightSystem &) = delete;
    HelloPointLightSystem(HelloPointLightSystem &&) = delete;
    HelloPointLightSystem &operator=(HelloPointLightSystem &&) = delete;
    
    void render(HelloVulkanFrameInfo &frameInfo);
    void update(HelloVulkanFrameInfo &frameInfo, GlobalUbo &ubo);
    
private:
    void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
    void createPipeline(VkRenderPass renderPass);
    
    HelloVulkanDevice &vulkanDevice;
    
    std::unique_ptr<HelloVulkanPipeline> vulkanPipeline;
    VkPipelineLayout pipelineLayout;
    
    
    const std::string shadersPath = SHADERS_DIR;
};

#endif /* HelloPointLightSystem_hpp */
