//
//  HelloVulkanRenderer.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 04/11/22.
//

#include "HelloVulkanRenderer.hpp"

#include <array>
#include <cassert>
#include <stdexcept>

HelloVulkanRenderer::HelloVulkanRenderer(HelloVulkanWindow& window, HelloVulkanDevice& device)
: vulkanWindow(window), vulkanDevice(device) {
    vulkanDevice.setMSAA(vulkanDevice.getMaxUsableSampleCount());
    recreateSwapChain();
    createCommandBuffers();
}

HelloVulkanRenderer::~HelloVulkanRenderer() { freeCommandBuffers(); }

void HelloVulkanRenderer::recreateSwapChain() {
    auto extent = vulkanWindow.getExtent();
    while (extent.width == 0 || extent.height == 0) {
        extent = vulkanWindow.getExtent();
        glfwWaitEvents();
    }
    vkDeviceWaitIdle(vulkanDevice.device());
    
    if (vulkanSwapChain == nullptr) {
        if (vulkanDevice.getMSAA() == VK_SAMPLE_COUNT_1_BIT)
            vulkanSwapChain = std::make_unique<HelloVulkanSimpleSwapChain>(vulkanDevice, extent);
        else
            vulkanSwapChain = std::make_unique<HelloVulkanMultiSamplingSwapChain>(vulkanDevice, extent);
    } else {
        std::shared_ptr<HelloVulkanSwapChain> oldSwapChain = std::move(vulkanSwapChain);
        if (vulkanDevice.getMSAA() == VK_SAMPLE_COUNT_1_BIT)
            vulkanSwapChain = std::make_unique<HelloVulkanSimpleSwapChain>(vulkanDevice, extent, oldSwapChain);
        else
            vulkanSwapChain = std::make_unique<HelloVulkanMultiSamplingSwapChain>(vulkanDevice, extent, oldSwapChain);
        if (!oldSwapChain->compareSwapFormats(*vulkanSwapChain.get())) {
            throw std::runtime_error("Swap chain image(or depth) format has changed!");
        }
    }
}

void HelloVulkanRenderer::createCommandBuffers() {
    commandBuffers.resize(HelloVulkanSwapChain::MAX_FRAMES_IN_FLIGHT);
    
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = vulkanDevice.getCommandPool();
    allocInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());
    
    if (vkAllocateCommandBuffers(vulkanDevice.device(), &allocInfo, commandBuffers.data()) !=
        VK_SUCCESS) {
        throw std::runtime_error("failed to allocate command buffers!");
    }
}

void HelloVulkanRenderer::freeCommandBuffers() {
    vkFreeCommandBuffers(
                         vulkanDevice.device(),
                         vulkanDevice.getCommandPool(),
                         static_cast<uint32_t>(commandBuffers.size()),
                         commandBuffers.data());
    commandBuffers.clear();
}

VkCommandBuffer HelloVulkanRenderer::beginFrame() {
    assert(!isFrameStarted && "Can't call beginFrame while already in progress");
    
    auto result = vulkanSwapChain->acquireNextImage(&currentImageIndex);
    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        recreateSwapChain();
        return nullptr;
    }
    
    if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        throw std::runtime_error("failed to acquire swap chain image!");
    }
    
    isFrameStarted = true;
    
    auto commandBuffer = getCurrentCommandBuffer();
    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    
    if (vkBeginCommandBuffer(commandBuffer, &beginInfo) != VK_SUCCESS) {
        throw std::runtime_error("failed to begin recording command buffer!");
    }
    return commandBuffer;
}

void HelloVulkanRenderer::endFrame() {
    assert(isFrameStarted && "Can't call endFrame while frame is not in progress");
    auto commandBuffer = getCurrentCommandBuffer();
    if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS) {
        throw std::runtime_error("failed to record command buffer!");
    }
    
    auto result = vulkanSwapChain->submitCommandBuffers(&commandBuffer, &currentImageIndex);
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR ||
        vulkanWindow.wasWindowResized()) {
        vulkanWindow.resetWindowResizedFlag();
        recreateSwapChain();
    } else if (result != VK_SUCCESS) {
        throw std::runtime_error("failed to present swap chain image!");
    }
    
    isFrameStarted = false;
    currentFrameIndex = (currentFrameIndex + 1) % HelloVulkanSwapChain::MAX_FRAMES_IN_FLIGHT;
}

void HelloVulkanRenderer::beginSwapChainRenderPass(VkCommandBuffer commandBuffer) {
    assert(isFrameStarted && "Can't call beginSwapChainRenderPass if frame is not in progress");
    assert(
           commandBuffer == getCurrentCommandBuffer() &&
           "Can't begin render pass on command buffer from a different frame");
    
    VkRenderPassBeginInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = vulkanSwapChain->getRenderPass();
    renderPassInfo.framebuffer = vulkanSwapChain->getFrameBuffer(currentImageIndex);
    
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = vulkanSwapChain->getSwapChainExtent();
    
    std::array<VkClearValue, 2> clearValues{};
    clearValues[0].color = {0.01f, 0.01f, 0.01f, 1.0f};
    clearValues[1].depthStencil = {1.0f, 0};
    renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
    renderPassInfo.pClearValues = clearValues.data();
    
    vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
    
    VkViewport viewport{};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = static_cast<float>(vulkanSwapChain->getSwapChainExtent().width);
    viewport.height = static_cast<float>(vulkanSwapChain->getSwapChainExtent().height);
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;
    VkRect2D scissor{{0, 0}, vulkanSwapChain->getSwapChainExtent()};
    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);
}

void HelloVulkanRenderer::endSwapChainRenderPass(VkCommandBuffer commandBuffer) {
    assert(isFrameStarted && "Can't call endSwapChainRenderPass if frame is not in progress");
    assert(commandBuffer == getCurrentCommandBuffer() &&
           "Can't end render pass on command buffer from a different frame");
    vkCmdEndRenderPass(commandBuffer);
}

