//
//  HelloVulkanApplication.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 20/10/22.
//



#include "HelloVulkanApplication.hpp"
// libs
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

#include <array>
#include <stdexcept>
#include <chrono>

#include "HelloGLFWKeyboardMovementController.hpp"
#include "HelloVulkanModels/HelloVulkanIndexedModel.hpp"
#include "HelloVulkanFrameInfo.hpp"

HelloVulkanApplication::HelloVulkanApplication(uint32_t width,
                                               uint32_t height,
                                               std::string windows_title)
: vulkanWindow(width, height, windows_title, true),
  vulkanDevice(vulkanWindow),
  vulkanRenderer(vulkanWindow, vulkanDevice)
{
    globalPool =
    HelloVulkanDescriptorPoolBuilder(vulkanDevice).setMaxSets(HelloVulkanSwapChain::MAX_FRAMES_IN_FLIGHT)
                                                  .addPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, HelloVulkanSwapChain::MAX_FRAMES_IN_FLIGHT)
                                                  .build();
    loadGameObjects();
}

HelloVulkanApplication::~HelloVulkanApplication(){}

void HelloVulkanApplication::run() {
    std::cout << "Main loop" << std::endl;
    HelloVulkanCamera camera{};
    HelloGLFWKeyboardMovementController cameraController{};
    
    std::vector<std::unique_ptr<HelloVulkanBuffer>> uboBuffers(HelloVulkanSwapChain::MAX_FRAMES_IN_FLIGHT);
    for (int i = 0; i < uboBuffers.size(); i++) {
        uboBuffers[i] = std::make_unique<HelloVulkanBuffer>(vulkanDevice,
                                                            sizeof(GlobalUbo),
                                                            1,
                                                            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                                                            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
        uboBuffers[i]->map();
    }
    
    auto globalSetLayout =
    HelloVulkanDescriptorSetLayoutBuilder(vulkanDevice).addBinding(0,
                                                                   VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                                                                   VK_SHADER_STAGE_ALL_GRAPHICS)
                                                       .build();
    
    std::vector<VkDescriptorSet> globalDescriptorSets(HelloVulkanSwapChain::MAX_FRAMES_IN_FLIGHT);
    for (int i = 0; i < globalDescriptorSets.size(); i++) {
        auto bufferInfo = uboBuffers[i]->descriptorInfo();
        HelloVulkanDescriptorWriter(*globalSetLayout, *globalPool).writeBuffer(0, &bufferInfo)
                                                                  .build(globalDescriptorSets[i]);
    }
    
    HelloRenderSystem helloRenderSystem(vulkanDevice,
                                        vulkanRenderer.getSwapChainRenderPass(),
                                        globalSetLayout->getDescriptorSetLayout());
    HelloPointLightSystem helloPointLightSystem(vulkanDevice,
                                        vulkanRenderer.getSwapChainRenderPass(),
                                        globalSetLayout->getDescriptorSetLayout());
    
    auto viewerObject = HelloVulkanGameObject::createGameObject();
    viewerObject.transform.translation.z = -2.5f;
    auto currentTime = std::chrono::high_resolution_clock::now();
    
    while (!vulkanWindow.shouldClose()) {
        glfwPollEvents();
        
        auto newTime = std::chrono::high_resolution_clock::now();
        float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
        currentTime = newTime;
        
        cameraController.moveInPlaneXZ(vulkanWindow.getGLFWwindow(), frameTime, viewerObject);
        camera.setViewYXZ(viewerObject.transform.translation, viewerObject.transform.rotation);
        
        float aspect = vulkanRenderer.getAspectRatio();
        //camera.setOrthographicProjection(-aspect, aspect, -1, 1, -1, 1);
        camera.setPerspectiveProjection(glm::radians(50.f), aspect, 0.1f, 10.f);
        if (auto commandBuffer = vulkanRenderer.beginFrame()) {
            int frameIndex = vulkanRenderer.getFrameIndex();
            HelloVulkanFrameInfo frameInfo{
                frameIndex,
                frameTime,
                commandBuffer,
                camera,
                globalDescriptorSets[frameIndex],
                gameObjects
            };
            
            // update
            GlobalUbo ubo{};
            ubo.projection = camera.getProjection();
            ubo.view = camera.getView();
            ubo.inverseView = camera.getInverseView();
            helloPointLightSystem.update(frameInfo, ubo);
            uboBuffers[frameIndex]->writeToBuffer(&ubo);
            uboBuffers[frameIndex]->flush();
            
            // order here matters (Blend effect)
            vulkanRenderer.beginSwapChainRenderPass(commandBuffer);
            helloRenderSystem.renderGameObjects(frameInfo);
            helloPointLightSystem.render(frameInfo);
            vulkanRenderer.endSwapChainRenderPass(commandBuffer);
            vulkanRenderer.endFrame();
        }
    }
    
    vkDeviceWaitIdle(vulkanDevice.device());
}

void HelloVulkanApplication::loadGameObjects() {
    std::shared_ptr<HelloVulkanModel> objModel = HelloVulkanIndexedModel::createModelFromFile(vulkanDevice, MODELS_DIR + std::string("flat_vase.obj"));
    auto flatVase = HelloVulkanGameObject::createGameObject();
    flatVase.model = objModel;
    flatVase.transform.translation = {-.5f, .5f, 0.f};
    flatVase.transform.scale = {1.f, 1.f, 1.f};
    gameObjects.emplace(flatVase.getId(), std::move(flatVase));
    
    objModel = HelloVulkanIndexedModel::createModelFromFile(vulkanDevice, MODELS_DIR + std::string("smooth_vase.obj") );
    auto smoothVase = HelloVulkanGameObject::createGameObject();
    smoothVase.model = objModel;
    smoothVase.transform.translation = {.5f, .5f, 0.f};
    smoothVase.transform.scale = {3.f, 1.5f, 3.f};
    gameObjects.emplace(smoothVase.getId(), std::move(smoothVase));
    
    objModel = HelloVulkanIndexedModel::createModelFromFile(vulkanDevice, MODELS_DIR + std::string("quad.obj"));
    auto floor = HelloVulkanGameObject::createGameObject();
    floor.model = objModel;
    floor.transform.translation = {0.f, .5f, 0.f};
    floor.transform.scale = {3.f, 1.f, 3.f};
    gameObjects.emplace(floor.getId(), std::move(floor));
    
    //Lights
    std::vector<glm::vec3> lightColors{
        {1.f, .1f, .1f},
        {.1f, .1f, 1.f},
        {.1f, 1.f, .1f},
        {1.f, 1.f, .1f},
        {.1f, 1.f, 1.f},
        {1.f, 1.f, 1.f}  //
    };
    
    for (int i = 0; i < lightColors.size(); i++) {
        auto pointLight = HelloVulkanGameObject::makePointLight(0.2f);
        pointLight.color = lightColors[i];
        auto rotateLight = glm::rotate(glm::mat4(1.f),
                                       (i * glm::two_pi<float>()) / lightColors.size(),
                                       {0.f, -1.f, 0.f});
        pointLight.transform.translation = glm::vec3(rotateLight * glm::vec4(-1.f, -1.f, -1.f, 1.f));
        gameObjects.emplace(pointLight.getId(), std::move(pointLight));
    }
}

