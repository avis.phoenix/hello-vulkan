//
//  HelloVulkanGameObject.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 03/11/22.
//

#include "HelloVulkanGameObject.hpp"

unsigned int HelloVulkanGameObject::currentId = 0;

HelloVulkanGameObject::HelloVulkanGameObject(uint idObj): id(idObj){
}

HelloVulkanGameObject HelloVulkanGameObject::createGameObject() {
    return HelloVulkanGameObject(currentId++);
}

HelloVulkanGameObject HelloVulkanGameObject::makePointLight(float intensity,
                                                            float radius,
                                                            glm::vec3 color) {
    HelloVulkanGameObject gameObj = HelloVulkanGameObject::createGameObject();
    gameObj.color = color;
    gameObj.transform.scale.x = radius;
    gameObj.pointLight = std::make_unique<PointLightComponent>();
    gameObj.pointLight->lightIntensity = intensity;
    return gameObj;
}
