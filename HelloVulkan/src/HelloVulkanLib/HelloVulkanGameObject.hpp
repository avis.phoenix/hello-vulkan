//
//  HelloVulkanGameObject.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 03/11/22.
//

#ifndef HelloVulkanGameObject_hpp
#define HelloVulkanGameObject_hpp

#include "HelloVulkanTransformations/HelloVulkan3DTransformation.hpp"
#include "HelloVulkanModels/HelloVulkanModel.hpp"

// libs
#include <glm/gtc/matrix_transform.hpp>

// std
#include <memory>
#include <unordered_map>

struct PointLightComponent {
  float lightIntensity = 1.0f;
};

class HelloVulkanGameObject{
public:
    using uint = unsigned int;
    using Map = std::unordered_map<uint, HelloVulkanGameObject>;
    
    static HelloVulkanGameObject createGameObject();
    static HelloVulkanGameObject makePointLight(float intensity = 10.f,
                                                float radius = 0.1f,
                                                glm::vec3 color = glm::vec3(1.f));
    
    HelloVulkanGameObject(const HelloVulkanGameObject &) = delete;
    HelloVulkanGameObject &operator=(const HelloVulkanGameObject &) = delete;
    HelloVulkanGameObject(HelloVulkanGameObject &&) = default;
    HelloVulkanGameObject &operator=(HelloVulkanGameObject &&) = default;

    uint getId() { return id; }
    std::shared_ptr<HelloVulkanModel> model{};
    glm::vec3 color{};
    HelloVulkan3DTransformation transform{};
    
    // Optional pointer components
    std::unique_ptr<PointLightComponent> pointLight = nullptr;
private:
    static uint currentId;
    uint id;
    HelloVulkanGameObject(uint id);
};
#endif /* HelloVulkanGameObject_hpp */
