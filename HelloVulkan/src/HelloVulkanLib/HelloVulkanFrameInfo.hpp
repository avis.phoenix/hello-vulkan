//
//  HelloVulkanFrameInfo.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 10/11/22.
//

#ifndef HelloVulkanFrameInfo_hpp
#define HelloVulkanFrameInfo_hpp

#include <vulkan/vulkan.h>

#include "HelloVulkanCamera.hpp"
#include "HelloVulkanGameObject.hpp"

#define MAX_LIGHTS 10

struct PointLight {
    glm::vec4 position{};  // ignore w
    glm::vec4 color{};     // w is intensity
};

struct GlobalUbo {
    glm::mat4 projection{1.f};
    glm::mat4 view{1.f};
    glm::mat4 inverseView{1.f};
    glm::vec4 ambientLightColor{1.f, 1.f, 1.f, .02f};  // w is intensity
    PointLight pointLights[MAX_LIGHTS];
    int numLights;
};

struct HelloVulkanFrameInfo {
    int frameIndex;
    float frameTime;
    VkCommandBuffer commandBuffer;
    HelloVulkanCamera &camera;
    VkDescriptorSet globalDescriptorSet;
    HelloVulkanGameObject::Map &gameObjects;
};

#endif /* HelloVulkanFrameInfo_hpp */
