//
//  HelloVulkanDefaultPaths.h
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 16/11/22.
//

#ifndef HelloVulkanDefaultPaths_h
#define HelloVulkanDefaultPaths_h


#ifndef SHADERS_DIR
#define SHADERS_DIR "shaders/"
#endif

#ifndef MODELS_DIR
#define MODELS_DIR "models/"
#endif

#endif /* HelloVulkanDefaultPaths_h */
