//
//  HelloVulkanDescriptorSetLayout.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 10/11/22.
//

#include "HelloVulkanDescriptorSetLayout.hpp"


HelloVulkanDescriptorSetLayout::HelloVulkanDescriptorSetLayout(HelloVulkanDevice &vulkanDevice,
                                                               std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings)
: vulkanDevice{vulkanDevice}, bindings{bindings} {
    std::vector<VkDescriptorSetLayoutBinding> setLayoutBindings{};
    for (auto kv : bindings) {
        setLayoutBindings.push_back(kv.second);
    }
    
    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutInfo{};
    descriptorSetLayoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutInfo.bindingCount = static_cast<uint32_t>(setLayoutBindings.size());
    descriptorSetLayoutInfo.pBindings = setLayoutBindings.data();
    
    if (vkCreateDescriptorSetLayout(vulkanDevice.device(),
                                    &descriptorSetLayoutInfo,
                                    nullptr,
                                    &descriptorSetLayout) != VK_SUCCESS) {
                                        throw std::runtime_error("failed to create descriptor set layout!");
                                    }
}

HelloVulkanDescriptorSetLayout::~HelloVulkanDescriptorSetLayout() {
    vkDestroyDescriptorSetLayout(vulkanDevice.device(), descriptorSetLayout, nullptr);
}
