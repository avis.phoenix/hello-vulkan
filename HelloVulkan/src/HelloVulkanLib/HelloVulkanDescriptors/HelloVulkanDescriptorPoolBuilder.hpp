//
//  HelloVulkanDescriptorPoolBuilder.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 11/11/22.
//

#ifndef HelloVulkanDescriptorPoolBuilder_hpp
#define HelloVulkanDescriptorPoolBuilder_hpp

#include "HelloVulkanDescriptorPool.hpp"

class HelloVulkanDescriptorPoolBuilder {
public:
    HelloVulkanDescriptorPoolBuilder(HelloVulkanDevice &vulkanDevice) : vulkanDevice{vulkanDevice} {}
    
    HelloVulkanDescriptorPoolBuilder &addPoolSize(VkDescriptorType descriptorType, uint32_t count);
    HelloVulkanDescriptorPoolBuilder &setPoolFlags(VkDescriptorPoolCreateFlags flags);
    HelloVulkanDescriptorPoolBuilder &setMaxSets(uint32_t count);
    std::unique_ptr<HelloVulkanDescriptorPool> build() const;
    
private:
    HelloVulkanDevice &vulkanDevice;
    std::vector<VkDescriptorPoolSize> poolSizes{};
    uint32_t maxSets = 1000;
    VkDescriptorPoolCreateFlags poolFlags = 0;
};

#endif /* HelloVulkanDescriptorPoolBuilder_hpp */
