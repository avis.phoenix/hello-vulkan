//
//  HelloVulkanDescriptorWriter.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 11/11/22.
//

#ifndef HelloVulkanDescriptorWriter_hpp
#define HelloVulkanDescriptorWriter_hpp

#include <vulkan/vulkan.h>

#include <vector>

#include "HelloVulkanDescriptorPool.hpp"
#include "HelloVulkanDescriptorSetLayout.hpp"

class HelloVulkanDescriptorWriter {
public:
    HelloVulkanDescriptorWriter(HelloVulkanDescriptorSetLayout &setLayout, HelloVulkanDescriptorPool &pool);
    
    HelloVulkanDescriptorWriter &writeBuffer(uint32_t binding, VkDescriptorBufferInfo *bufferInfo);
    HelloVulkanDescriptorWriter &writeImage(uint32_t binding, VkDescriptorImageInfo *imageInfo);
    
    bool build(VkDescriptorSet &set);
    void overwrite(VkDescriptorSet &set);
    
private:
    HelloVulkanDescriptorSetLayout &setLayout;
    HelloVulkanDescriptorPool &pool;
    std::vector<VkWriteDescriptorSet> writes;
};

#endif /* HelloVulkanDescriptorWriter_hpp */
