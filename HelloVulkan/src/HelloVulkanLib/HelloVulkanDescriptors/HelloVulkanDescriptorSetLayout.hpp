//
//  HelloVulkanDescriptorSetLayout.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 10/11/22.
//

#ifndef HelloVulkanDescriptorSetLayout_hpp
#define HelloVulkanDescriptorSetLayout_hpp

#include <vulkan/vulkan.h>
// std
#include <memory>
#include <unordered_map>
#include <vector>

#include "HelloVulkanDevice.hpp"

class HelloVulkanDescriptorSetLayout {
public:
    HelloVulkanDescriptorSetLayout(HelloVulkanDevice &vulkanDevice,
                                   std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings);
    ~HelloVulkanDescriptorSetLayout();
    
    HelloVulkanDescriptorSetLayout(const HelloVulkanDescriptorSetLayout &) = delete;
    HelloVulkanDescriptorSetLayout &operator=(const HelloVulkanDescriptorSetLayout &) = delete;
    
    VkDescriptorSetLayout getDescriptorSetLayout() const { return descriptorSetLayout; }
    
private:
    HelloVulkanDevice &vulkanDevice;
    VkDescriptorSetLayout descriptorSetLayout;
    std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings;
    
    friend class HelloVulkanDescriptorWriter;
};

#endif /* HelloVulkanDescriptorSetLayout_hpp */
