//
//  HelloVulkanDescriptorSetLayoutBuilder.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 11/11/22.
//

#ifndef HelloVulkanDescriptorSetLayoutBuilder_hpp
#define HelloVulkanDescriptorSetLayoutBuilder_hpp

#include "HelloVulkanDescriptorSetLayout.hpp"

class HelloVulkanDescriptorSetLayoutBuilder {
public:
    HelloVulkanDescriptorSetLayoutBuilder(HelloVulkanDevice &vulkanDevice) : vulkanDevice{vulkanDevice} {}
    
    HelloVulkanDescriptorSetLayoutBuilder &addBinding(uint32_t binding,
                                                      VkDescriptorType descriptorType,
                                                      VkShaderStageFlags stageFlags,
                                                      uint32_t count = 1);
    std::unique_ptr<HelloVulkanDescriptorSetLayout> build() const;
    
private:
    HelloVulkanDevice &vulkanDevice;
    std::unordered_map<uint32_t, VkDescriptorSetLayoutBinding> bindings{};
};

#endif /* HelloVulkanDescriptorSetLayoutBuilder_hpp */
