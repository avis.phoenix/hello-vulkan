//
//  HelloVulkanDescriptorPool.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 11/11/22.
//

#ifndef HelloVulkanDescriptorPool_hpp
#define HelloVulkanDescriptorPool_hpp

#include <vulkan/vulkan.h>

#include <memory>
#include <vector>

#include "HelloVulkanDevice.hpp"

class HelloVulkanDescriptorPool {
public:
    HelloVulkanDescriptorPool(HelloVulkanDevice &vulkanDevice,
                              uint32_t maxSets,
                              VkDescriptorPoolCreateFlags poolFlags,
                              const std::vector<VkDescriptorPoolSize> &poolSizes);
    ~HelloVulkanDescriptorPool();
    HelloVulkanDescriptorPool(const HelloVulkanDescriptorPool &) = delete;
    HelloVulkanDescriptorPool &operator=(const HelloVulkanDescriptorPool &) = delete;
    
    bool allocateDescriptor(const VkDescriptorSetLayout descriptorSetLayout,
                            VkDescriptorSet &descriptor) const;
    
    void freeDescriptors(std::vector<VkDescriptorSet> &descriptors) const;
    
    void resetPool();
    
private:
    HelloVulkanDevice &vulkanDevice;
    VkDescriptorPool descriptorPool;
    
    friend class HelloVulkanDescriptorWriter;
};


#endif /* HelloVulkanDescriptorPool_hpp */
