//
//  HelloVulkanDescriptorSetLayoutBuilder.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 11/11/22.
//

#include "HelloVulkanDescriptorSetLayoutBuilder.hpp"
#include <cassert>

HelloVulkanDescriptorSetLayoutBuilder &HelloVulkanDescriptorSetLayoutBuilder::addBinding(uint32_t binding,
                                                                                         VkDescriptorType descriptorType,
                                                                                         VkShaderStageFlags stageFlags,
                                                                                         uint32_t count) {
    assert(bindings.count(binding) == 0 && "Binding already in use");
    VkDescriptorSetLayoutBinding layoutBinding{};
    layoutBinding.binding = binding;
    layoutBinding.descriptorType = descriptorType;
    layoutBinding.descriptorCount = count;
    layoutBinding.stageFlags = stageFlags;
    bindings[binding] = layoutBinding;
    return *this;
}

std::unique_ptr<HelloVulkanDescriptorSetLayout> HelloVulkanDescriptorSetLayoutBuilder::build() const {
    return std::make_unique<HelloVulkanDescriptorSetLayout>(vulkanDevice, bindings);
}
