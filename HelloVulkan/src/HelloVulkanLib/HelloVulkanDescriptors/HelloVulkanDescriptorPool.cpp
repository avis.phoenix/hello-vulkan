//
//  HelloVulkanDescriptorPool.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 11/11/22.
//

#include "HelloVulkanDescriptorPool.hpp"

HelloVulkanDescriptorPool::HelloVulkanDescriptorPool(HelloVulkanDevice &vulkanDevice,
                                                     uint32_t maxSets,
                                                     VkDescriptorPoolCreateFlags poolFlags,
                                                     const std::vector<VkDescriptorPoolSize> &poolSizes)
: vulkanDevice{vulkanDevice} {
    VkDescriptorPoolCreateInfo descriptorPoolInfo{};
    descriptorPoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
    descriptorPoolInfo.pPoolSizes = poolSizes.data();
    descriptorPoolInfo.maxSets = maxSets;
    descriptorPoolInfo.flags = poolFlags;
    
    if (vkCreateDescriptorPool(vulkanDevice.device(), &descriptorPoolInfo, nullptr, &descriptorPool) !=
        VK_SUCCESS) {
        throw std::runtime_error("failed to create descriptor pool!");
    }
}

HelloVulkanDescriptorPool::~HelloVulkanDescriptorPool() {
    vkDestroyDescriptorPool(vulkanDevice.device(), descriptorPool, nullptr);
}

bool HelloVulkanDescriptorPool::allocateDescriptor(const VkDescriptorSetLayout descriptorSetLayout,
                                                   VkDescriptorSet &descriptor) const {
    VkDescriptorSetAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = descriptorPool;
    allocInfo.pSetLayouts = &descriptorSetLayout;
    allocInfo.descriptorSetCount = 1;
    
    // Might want to create a "DescriptorPoolManager" class that handles this case, and builds
    // a new pool whenever an old pool fills up. But this is beyond our current scope
    if (vkAllocateDescriptorSets(vulkanDevice.device(), &allocInfo, &descriptor) != VK_SUCCESS) {
        return false;
    }
    return true;
}

void HelloVulkanDescriptorPool::freeDescriptors(std::vector<VkDescriptorSet> &descriptors) const {
    vkFreeDescriptorSets(vulkanDevice.device(),
                         descriptorPool,
                         static_cast<uint32_t>(descriptors.size()),
                         descriptors.data());
}

void HelloVulkanDescriptorPool::resetPool() {
    vkResetDescriptorPool(vulkanDevice.device(), descriptorPool, 0);
}
