//
//  HelloVulkanDescriptorPoolHelloVulkanDescriptorPoolBuilder.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 11/11/22.
//

#include "HelloVulkanDescriptorPoolBuilder.hpp"
#include "HelloVulkanDescriptorPool.hpp"


HelloVulkanDescriptorPoolBuilder &HelloVulkanDescriptorPoolBuilder::addPoolSize(VkDescriptorType descriptorType,
                                                                                                   uint32_t count) {
  poolSizes.push_back({descriptorType, count});
  return *this;
}

HelloVulkanDescriptorPoolBuilder &HelloVulkanDescriptorPoolBuilder::setPoolFlags(VkDescriptorPoolCreateFlags flags) {
  poolFlags = flags;
  return *this;
}

HelloVulkanDescriptorPoolBuilder &HelloVulkanDescriptorPoolBuilder::setMaxSets(uint32_t count) {
  maxSets = count;
  return *this;
}

std::unique_ptr<HelloVulkanDescriptorPool> HelloVulkanDescriptorPoolBuilder::build() const {
    return std::make_unique<HelloVulkanDescriptorPool>(vulkanDevice, maxSets, poolFlags, poolSizes);
}
