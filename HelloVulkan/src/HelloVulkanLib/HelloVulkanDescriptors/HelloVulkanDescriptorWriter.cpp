//
//  HelloVulkanDescriptorWriter.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 11/11/22.
//

#include "HelloVulkanDescriptorWriter.hpp"
#include <cassert>

HelloVulkanDescriptorWriter::HelloVulkanDescriptorWriter(HelloVulkanDescriptorSetLayout &setLayout,
                                                         HelloVulkanDescriptorPool &pool)
: setLayout{setLayout}, pool{pool} {}

HelloVulkanDescriptorWriter &HelloVulkanDescriptorWriter::writeBuffer(
                                                                      uint32_t binding, VkDescriptorBufferInfo *bufferInfo) {
    assert(setLayout.bindings.count(binding) == 1 && "Layout does not contain specified binding");
    
    auto &bindingDescription = setLayout.bindings[binding];
    
    assert(
           bindingDescription.descriptorCount == 1 &&
           "Binding single descriptor info, but binding expects multiple");
    
    VkWriteDescriptorSet write{};
    write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write.descriptorType = bindingDescription.descriptorType;
    write.dstBinding = binding;
    write.pBufferInfo = bufferInfo;
    write.descriptorCount = 1;
    
    writes.push_back(write);
    return *this;
}

HelloVulkanDescriptorWriter &HelloVulkanDescriptorWriter::writeImage(uint32_t binding,
                                                                     VkDescriptorImageInfo *imageInfo) {
    assert(setLayout.bindings.count(binding) == 1 && "Layout does not contain specified binding");
    
    auto &bindingDescription = setLayout.bindings[binding];
    
    assert(bindingDescription.descriptorCount == 1 &&
           "Binding single descriptor info, but binding expects multiple");
    
    VkWriteDescriptorSet write{};
    write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write.descriptorType = bindingDescription.descriptorType;
    write.dstBinding = binding;
    write.pImageInfo = imageInfo;
    write.descriptorCount = 1;
    
    writes.push_back(write);
    return *this;
}

bool HelloVulkanDescriptorWriter::build(VkDescriptorSet &set) {
    bool success = pool.allocateDescriptor(setLayout.getDescriptorSetLayout(), set);
    if (!success) {
        return false;
    }
    overwrite(set);
    return true;
}

void HelloVulkanDescriptorWriter::overwrite(VkDescriptorSet &set) {
    for (auto &write : writes) {
        write.dstSet = set;
    }
    vkUpdateDescriptorSets(pool.vulkanDevice.device(), static_cast<uint32_t>(writes.size()), writes.data(), 0, nullptr);
}

