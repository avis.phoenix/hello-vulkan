//
//  HelloVulkanWindow.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 28/10/22.
//

#ifndef HelloVulkanWindow_hpp
#define HelloVulkanWindow_hpp

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <string>

class HelloVulkanWindow {
public:
    HelloVulkanWindow(uint32_t width, uint32_t height, std::string windows_title, bool resizable = false);
    ~HelloVulkanWindow();
    
    // Not copyable or movable
    HelloVulkanWindow(const HelloVulkanWindow &) = delete;
    HelloVulkanWindow &operator=(const HelloVulkanWindow &) = delete;
    HelloVulkanWindow(HelloVulkanWindow &&) = delete;
    HelloVulkanWindow &operator=(HelloVulkanWindow &&) = delete;
    
    bool shouldClose() { return glfwWindowShouldClose(window); }
    VkExtent2D getExtent() { return {static_cast<uint32_t>(WIDTH), static_cast<uint32_t>(HEIGHT)}; }
    GLFWwindow *getGLFWwindow() const { return window; }
    void createWindowSurface(VkInstance instance, VkSurfaceKHR *surface);
    
    uint32_t getWidth() { return WIDTH; }
    uint32_t getHeight() { return HEIGHT; }
    
    bool wasWindowResized() { return framebufferResized; }
    void resetWindowResizedFlag() { framebufferResized = false; }
private:
    int WIDTH;
    int HEIGHT;
    const std::string title;
    bool framebufferResized = false;
    
    GLFWwindow* window;
    
    void initWindow(bool resizable);
    
    void cleanup();
    
    static void framebufferResizeCallback(GLFWwindow *window, int width, int height);
};

#endif /* HelloVulkanWindow_hpp */
