//
//  HelloVulkanSwapChain.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 29/10/22.
//

#ifndef HelloVulkanSwapChain_hpp
#define HelloVulkanSwapChain_hpp

#include <string>
#include <vector>
#include <memory>
#include <vulkan/vulkan.h>

#include "HelloVulkanDevice.hpp"

class HelloVulkanSwapChain{
public:
    static constexpr int MAX_FRAMES_IN_FLIGHT = 2;
    
    void extracted();
    
    HelloVulkanSwapChain(HelloVulkanDevice &deviceRef, VkExtent2D windowExtent);
    HelloVulkanSwapChain(HelloVulkanDevice &deviceRef,
                         VkExtent2D windowExtent,
                         std::shared_ptr<HelloVulkanSwapChain> previous);
    virtual ~HelloVulkanSwapChain()=0;
    
    // Not copyable or movable
    HelloVulkanSwapChain(const HelloVulkanSwapChain &) = delete;
    HelloVulkanSwapChain &operator=(const HelloVulkanSwapChain &) = delete;
    HelloVulkanSwapChain(HelloVulkanSwapChain &&) = delete;
    HelloVulkanSwapChain &operator=(HelloVulkanSwapChain &&) = delete;
    
    VkFramebuffer getFrameBuffer(int index) { return swapChainFramebuffers[index]; }
    VkRenderPass getRenderPass() { return renderPass; }
    VkImageView getImageView(int index) { return swapChainImageViews[index]; }
    size_t imageCount() { return swapChainImages.size(); }
    VkFormat getSwapChainImageFormat() { return swapChainImageFormat; }
    VkExtent2D getSwapChainExtent() { return swapChainExtent; }
    uint32_t width() { return swapChainExtent.width; }
    uint32_t height() { return swapChainExtent.height; }

    float extentAspectRatio() {
        return static_cast<float>(swapChainExtent.width) / static_cast<float>(swapChainExtent.height);
    }
    VkFormat findDepthFormat();

    VkResult acquireNextImage(uint32_t *imageIndex);
    VkResult submitCommandBuffers(const VkCommandBuffer *buffers, uint32_t *imageIndex);
    
    bool compareSwapFormats(const HelloVulkanSwapChain &swapChain) const {
        return swapChain.swapChainDepthFormat == swapChainDepthFormat &&
               swapChain.swapChainImageFormat == swapChainImageFormat;
    }

protected:
    void createSwapChain();
    void createImageViews();
    virtual void createDepthResources();
    virtual void createRenderPass();
    virtual void createFramebuffers();
    void createSyncObjects();
    virtual void init() = 0;
    
    void normalConstructor();
    void swapChainConstructor();

    // Helper functions
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(
      const std::vector<VkSurfaceFormatKHR> &availableFormats);
    VkPresentModeKHR chooseSwapPresentMode(
      const std::vector<VkPresentModeKHR> &availablePresentModes);
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities);

    VkFormat swapChainImageFormat;
    VkFormat swapChainDepthFormat;
    VkExtent2D swapChainExtent;

    std::vector<VkFramebuffer> swapChainFramebuffers;
    VkRenderPass renderPass;

    std::vector<VkImage> depthImages;
    std::vector<VkDeviceMemory> depthImageMemorys;
    std::vector<VkImageView> depthImageViews;
    std::vector<VkImage> swapChainImages;
    std::vector<VkImageView> swapChainImageViews;

    HelloVulkanDevice &device;
    VkExtent2D windowExtent;

    VkSwapchainKHR swapChain;
    std::shared_ptr<HelloVulkanSwapChain> oldSwapChain;

    std::vector<VkSemaphore> imageAvailableSemaphores;
    std::vector<VkSemaphore> renderFinishedSemaphores;
    std::vector<VkFence> inFlightFences;
    std::vector<VkFence> imagesInFlight;
    size_t currentFrame = 0;
};

#endif /* HelloVulkanSwapChain_hpp */
