//
//  HelloVulkanSimpleSwapChain.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 23/11/22.
//

#ifndef HelloVulkanSimpleSwapChain_hpp
#define HelloVulkanSimpleSwapChain_hpp

#include "HelloVulkanDevice.hpp"
#include "HelloVulkanSwapChain.hpp"

class HelloVulkanSimpleSwapChain: public HelloVulkanSwapChain{
public:
    
    HelloVulkanSimpleSwapChain(HelloVulkanDevice &deviceRef,
                         VkExtent2D windowExtent);
    HelloVulkanSimpleSwapChain(HelloVulkanDevice &deviceRef,
                         VkExtent2D windowExtent,
                         std::shared_ptr<HelloVulkanSwapChain> previous);
    ~HelloVulkanSimpleSwapChain();
    
    // Not copyable or movable
    HelloVulkanSimpleSwapChain(const HelloVulkanSimpleSwapChain &) = delete;
    HelloVulkanSimpleSwapChain &operator=(const HelloVulkanSimpleSwapChain &) = delete;
    HelloVulkanSimpleSwapChain(HelloVulkanSimpleSwapChain &&) = delete;
    HelloVulkanSimpleSwapChain &operator=(HelloVulkanSimpleSwapChain &&) = delete;

protected:
    void init();
};
#endif /* HelloVulkanSimpleSwapChain_hpp */
