//
//  HelloVulkanSimpleSwapChains.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 23/11/22.
//

#include "HelloVulkanSimpleSwapChain.hpp"

HelloVulkanSimpleSwapChain::HelloVulkanSimpleSwapChain(HelloVulkanDevice &deviceRef,
                                                         VkExtent2D extent)
: HelloVulkanSwapChain{deviceRef, extent} {
    normalConstructor();
}

HelloVulkanSimpleSwapChain::HelloVulkanSimpleSwapChain(HelloVulkanDevice &deviceRef,
                                                         VkExtent2D extent,
                                                         std::shared_ptr<HelloVulkanSwapChain> previous)
: HelloVulkanSwapChain{deviceRef, extent, previous} {
    swapChainConstructor();
}

void HelloVulkanSimpleSwapChain::init() {
    createSwapChain();
    createImageViews();
    createRenderPass();
    createDepthResources();
    createFramebuffers();
    createSyncObjects();
}


HelloVulkanSimpleSwapChain::~HelloVulkanSimpleSwapChain() {
  for (auto imageView : swapChainImageViews) {
    vkDestroyImageView(device.device(), imageView, nullptr);
  }
  swapChainImageViews.clear();

  if (swapChain != nullptr) {
    vkDestroySwapchainKHR(device.device(), swapChain, nullptr);
    swapChain = nullptr;
  }

  for (int i = 0; i < depthImages.size(); i++) {
    vkDestroyImageView(device.device(), depthImageViews[i], nullptr);
    vkDestroyImage(device.device(), depthImages[i], nullptr);
    vkFreeMemory(device.device(), depthImageMemorys[i], nullptr);
  }

  for (auto framebuffer : swapChainFramebuffers) {
    vkDestroyFramebuffer(device.device(), framebuffer, nullptr);
  }

  vkDestroyRenderPass(device.device(), renderPass, nullptr);

  // cleanup synchronization objects
  for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
    vkDestroySemaphore(device.device(), renderFinishedSemaphores[i], nullptr);
    vkDestroySemaphore(device.device(), imageAvailableSemaphores[i], nullptr);
    vkDestroyFence(device.device(), inFlightFences[i], nullptr);
  }
}
