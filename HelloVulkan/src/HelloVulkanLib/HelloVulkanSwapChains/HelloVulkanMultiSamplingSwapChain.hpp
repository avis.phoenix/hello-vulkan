//
//  HelloVulkanMultiSamplingSwapChain.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 23/11/22.
//

#ifndef HelloVulkanMultiSamplingSwapChain_hpp
#define HelloVulkanMultiSamplingSwapChain_hpp

#include <string>
#include <vector>
#include <memory>
#include <vulkan/vulkan.h>

#include "HelloVulkanDevice.hpp"
#include "HelloVulkanSwapChain.hpp"

class HelloVulkanMultiSamplingSwapChain: public HelloVulkanSwapChain{
public:
    
    HelloVulkanMultiSamplingSwapChain(HelloVulkanDevice &deviceRef,
                         VkExtent2D windowExtent);
    HelloVulkanMultiSamplingSwapChain(HelloVulkanDevice &deviceRef,
                         VkExtent2D windowExtent,
                         std::shared_ptr<HelloVulkanSwapChain> previous);
    ~HelloVulkanMultiSamplingSwapChain();
    
    // Not copyable or movable
    HelloVulkanMultiSamplingSwapChain(const HelloVulkanMultiSamplingSwapChain &) = delete;
    HelloVulkanMultiSamplingSwapChain &operator=(const HelloVulkanMultiSamplingSwapChain &) = delete;
    HelloVulkanMultiSamplingSwapChain(HelloVulkanMultiSamplingSwapChain &&) = delete;
    HelloVulkanMultiSamplingSwapChain &operator=(HelloVulkanMultiSamplingSwapChain &&) = delete;

protected:
    void createColorResources();
    void createDepthResources();
    void createRenderPass();
    void createFramebuffers();
    void init();

    std::vector<VkImage> colorImages;
    std::vector<VkDeviceMemory> colorImageMemories;
    std::vector<VkImageView> colorImageViews;
};

#endif /* HelloVulkanMultiSamplingSwapChain_hpp */
