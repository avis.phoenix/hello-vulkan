//
//  HelloVulkan3DTransformation.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 07/11/22.
//

#ifndef HelloVulkan3DTransformation_hpp
#define HelloVulkan3DTransformation_hpp

#include <glm/ext.hpp>

class HelloVulkan3DTransformation{
public:
    glm::vec3 translation{};  // (position offset)
    glm::vec3 scale{1.f, 1.f, 1.f};
    glm::vec3 rotation{};
    
    glm::mat4 getMat4();
    glm::mat3 getNormalMatrix();
};

#endif /* HelloVulkan3DTransformation_hpp */
