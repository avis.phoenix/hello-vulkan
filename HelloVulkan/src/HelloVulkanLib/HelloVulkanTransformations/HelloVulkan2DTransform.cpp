//
//  HelloVulkan2DTransform.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 03/11/22.
//

#include "HelloVulkan2DTransform.hpp"

glm::mat2 HelloVulkan2DTransform::getMat2() {
    const float s = glm::sin(rotation);
    const float c = glm::cos(rotation);
    glm::mat2 rotMatrix{{c, s}, {-s, c}};
    
    glm::mat2 scaleMat{{scale.x, .0f}, {.0f, scale.y}};
    return rotMatrix * scaleMat;
}
