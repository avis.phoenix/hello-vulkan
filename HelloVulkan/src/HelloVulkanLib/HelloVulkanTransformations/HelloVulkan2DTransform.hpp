//
//  HelloVulkan2DTransform.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 03/11/22.
//

#ifndef HelloVulkan2DTransform_hpp
#define HelloVulkan2DTransform_hpp

#include <glm/ext.hpp>

class HelloVulkan2DTransform{
public:
    glm::vec2 translation{.0f, 0.f};  // (position offset)
    glm::vec2 scale{1.f, 1.f};
    float rotation;
    
    glm::mat2 getMat2();
};

#endif /* HelloVulkan2DTransform_hpp */
