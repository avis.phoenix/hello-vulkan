//
//  HelloVulkanApplication.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 20/10/22.
//

#ifndef HelloVulkanApplication_hpp
#define HelloVulkanApplication_hpp

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

#include <iostream>
#include <string>

#include "HelloVulkanWindow.hpp"
#include "HelloVulkanDevice.hpp"
#include "HelloVulkanPipeline.hpp"
#include "HelloVulkanRenderer.hpp"
#include "HelloVulkanGameObject.hpp"
#include "HelloRenderSystems/HelloRenderSystem.hpp"
#include "HelloRenderSystems/HelloPointLightSystem.hpp"
#include "HelloVulkanCamera.hpp"
#include "HelloVulkanDescriptors/HelloVulkanDescriptorPoolBuilder.hpp"
#include "HelloVulkanDescriptors/HelloVulkanDescriptorSetLayoutBuilder.hpp"
#include "HelloVulkanDescriptors/HelloVulkanDescriptorWriter.hpp"

#include "HelloVulkanDefaultPaths.h"

#include <memory>
#include <vector>

class HelloVulkanApplication {
public:
    void run();
    
    HelloVulkanApplication(uint32_t width, uint32_t height, std::string windows_title);
    ~HelloVulkanApplication();
    
    // Not copyable or movable
    HelloVulkanApplication(const HelloVulkanApplication &) = delete;
    HelloVulkanApplication &operator=(const HelloVulkanApplication &) = delete;
    HelloVulkanApplication(HelloVulkanApplication &&) = delete;
    HelloVulkanApplication &operator=(HelloVulkanApplication &&) = delete;

private:
    void loadGameObjects();
    
    HelloVulkanWindow vulkanWindow;
    HelloVulkanDevice vulkanDevice;
    HelloVulkanRenderer vulkanRenderer;
    
    // note: order of declarations matters
    std::unique_ptr<HelloVulkanDescriptorPool> globalPool{};
    HelloVulkanGameObject::Map gameObjects;
    
    std::string modelPath = MODELS_DIR;
};


#endif /* HelloVulkanApplication_hpp */
