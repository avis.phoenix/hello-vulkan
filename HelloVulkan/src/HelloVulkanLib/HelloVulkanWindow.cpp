//
//  HelloVulkanWindow.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 28/10/22.
//

#include "HelloVulkanWindow.hpp"

#include <iostream>
#include <stdexcept>

HelloVulkanWindow::HelloVulkanWindow(uint32_t width,
                                     uint32_t height,
                                     std::string windows_title,
                                     bool resizable)
: WIDTH(width),
  HEIGHT(height),
  title(windows_title) {
    initWindow(resizable);
}

HelloVulkanWindow::~HelloVulkanWindow(){
    cleanup();
}

void HelloVulkanWindow::initWindow(bool resizable) {
    std::cout << "Init Window" << std::endl;
    
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, resizable? GLFW_TRUE : GLFW_FALSE);

    window = glfwCreateWindow(WIDTH, HEIGHT, title.c_str(), nullptr, nullptr);
    glfwSetWindowUserPointer(window, this);
    glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
}

void HelloVulkanWindow::cleanup() {
    std::cout << "Clean Up!" << std::endl;
    glfwDestroyWindow(window);
    glfwTerminate();
}

void HelloVulkanWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR *surface){
    if (glfwCreateWindowSurface(instance, window, nullptr, surface)){
        throw std::runtime_error("failed to create window surface!");
    }
}

void HelloVulkanWindow::framebufferResizeCallback(GLFWwindow *window, int width, int height){
    auto vWindow = reinterpret_cast<HelloVulkanWindow *>(glfwGetWindowUserPointer(window));
    vWindow->framebufferResized = true;
    vWindow->WIDTH = width;
    vWindow->HEIGHT = height;
}
