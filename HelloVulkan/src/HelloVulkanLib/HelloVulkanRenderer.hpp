//
//  HelloVulkanRenderer.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 04/11/22.
//

#ifndef HelloVulkanRenderer_hpp
#define HelloVulkanRenderer_hpp

#include "HelloVulkanWindow.hpp"
#include "HelloVulkanDevice.hpp"
#include "HelloVulkanSwapChains/HelloVulkanSwapChain.hpp"
#include "HelloVulkanSwapChains/HelloVulkanMultiSamplingSwapChain.hpp"
#include "HelloVulkanSwapChains/HelloVulkanSimpleSwapChain.hpp"

#include <cassert>

class HelloVulkanRenderer{
public:
    HelloVulkanRenderer(HelloVulkanWindow &window, HelloVulkanDevice &device);
    ~HelloVulkanRenderer();

    // Not copyable or movable
    HelloVulkanRenderer(const HelloVulkanRenderer &) = delete;
    HelloVulkanRenderer &operator=(const HelloVulkanRenderer &) = delete;
    HelloVulkanRenderer(HelloVulkanRenderer &&) = delete;
    HelloVulkanRenderer &operator=(HelloVulkanRenderer &&) = delete;

    VkRenderPass getSwapChainRenderPass() const { return vulkanSwapChain->getRenderPass(); }
    float getAspectRatio() const { return vulkanSwapChain->extentAspectRatio(); }
    bool isFrameInProgress() const { return isFrameStarted; }

    VkCommandBuffer getCurrentCommandBuffer() const {
        assert(isFrameStarted && "Cannot get command buffer when frame not in progress");
        return commandBuffers[currentFrameIndex];
    }

    int getFrameIndex() const {
        assert(isFrameStarted && "Cannot get frame index when frame not in progress");
        return currentFrameIndex;
    }

    VkCommandBuffer beginFrame();
    void endFrame();
    void beginSwapChainRenderPass(VkCommandBuffer commandBuffer);
    void endSwapChainRenderPass(VkCommandBuffer commandBuffer);
private:
    void createCommandBuffers();
    void freeCommandBuffers();
    void recreateSwapChain();

    HelloVulkanWindow &vulkanWindow;
    HelloVulkanDevice &vulkanDevice;
    std::unique_ptr<HelloVulkanSwapChain> vulkanSwapChain;
    std::vector<VkCommandBuffer> commandBuffers;

    uint32_t currentImageIndex;
    int currentFrameIndex{0};
    bool isFrameStarted{false};
};

#endif /* HelloVulkanRenderer_hpp */
