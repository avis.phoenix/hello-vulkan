//
//  HelloVulkanPipeline.hpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 26/10/22.
//

#ifndef HelloVulkanPipeline_hpp
#define HelloVulkanPipeline_hpp

#include <string>
#include <vector>
#include <vulkan/vulkan.h>

#include "HelloVulkanDevice.hpp"
#include "HelloVulkanVertex.hpp"


struct PipelineConfigInfo {
    
    PipelineConfigInfo() = default;
    PipelineConfigInfo(const PipelineConfigInfo&) = delete;
    PipelineConfigInfo& operator=(const PipelineConfigInfo&) = delete;
    
    std::vector<VkVertexInputBindingDescription> bindingDescriptions{};
    std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};
    VkPipelineViewportStateCreateInfo viewportInfo;
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo;
    VkPipelineRasterizationStateCreateInfo rasterizationInfo;
    VkPipelineMultisampleStateCreateInfo multisampleInfo;
    VkPipelineColorBlendAttachmentState colorBlendAttachment;
    VkPipelineColorBlendStateCreateInfo colorBlendInfo;
    VkPipelineDepthStencilStateCreateInfo depthStencilInfo;
    std::vector<VkDynamicState> dynamicStateEnables;
    VkPipelineDynamicStateCreateInfo dynamicStateInfo;
    VkPipelineLayout pipelineLayout = nullptr;
    VkRenderPass renderPass = nullptr;
    uint32_t subpass = 0;
};

class HelloVulkanPipeline{
public:
    HelloVulkanPipeline(HelloVulkanDevice& device,
                        const std::string& vertFilepath,
                        const std::string& fragFilepath,
                        const PipelineConfigInfo& configInfo);
    ~HelloVulkanPipeline();
    
    // Not copyable or movable
    HelloVulkanPipeline(const HelloVulkanPipeline &) = delete;
    HelloVulkanPipeline &operator=(const HelloVulkanPipeline &) = delete;
    HelloVulkanPipeline(HelloVulkanPipeline &&) = delete;
    HelloVulkanPipeline &operator=(HelloVulkanPipeline &&) = delete;
    
    void bind(VkCommandBuffer commandBuffer);
    
    static void defaultPipelineConfigInfo(PipelineConfigInfo& configInfo);
    static void enableAlphaBlending(PipelineConfigInfo& configInfo);
    static void enableMultisampling(PipelineConfigInfo& configInfo, VkSampleCountFlagBits msaaSamples);
private:
    static std::vector<char> readFile(const std::string& filename);

    void createGraphicsPipeline(const std::string& vertFilepath,
                                const std::string& fragFilepath,
                                const PipelineConfigInfo& configInfo);
                                  
    void createShaderModule(const std::vector<char>& code, VkShaderModule* shaderModule);
    
    HelloVulkanDevice& vulkanDevice;
    VkPipeline graphicsPipeline;
    VkShaderModule vertShaderModule;
    VkShaderModule fragShaderModule;
};



#endif /* HelloVulkanPipeline_hpp */
