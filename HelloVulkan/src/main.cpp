//
//  main.cpp
//  HelloVulkan
//
//  Created by Gustavo Adolfo García Cano on 20/10/22.
//

#include <iostream>
#include "HelloVulkanApplication.hpp"

int main(int argc, const char * argv[]) {
    HelloVulkanApplication app(600, 480, "GLFW WIndow");
    try {
        app.run();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
