#!/bin/bash
mkdir -p build
cd build
cmake -S ../ -B .
make && make Shaders
cp -R ../HelloVulkan/models .
mkdir -p shaders
cp -R ../HelloVulkan/shaders/*.spv shaders/.
./HelloVulkan
cd ..