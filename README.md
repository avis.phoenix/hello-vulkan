# Hello Vulkan

This code it is my own implementation of the youtube tutorial videos: [Vulkan Game Engine](https://youtu.be/Y9U9IE0gVHA) by [Brendan Galea](https://www.youtube.com/c/BrendanGalea)
littleVulkanEngine GitHub: [https://github.com/blurrypiano/littleVulkanEngine/](https://github.com/blurrypiano/littleVulkanEngine/)

## Getting started
 In order to compile the project you will need the libraries:
 - [Vulkan SDK](https://vulkan.lunarg.com/)
 - [GLFW](https://www.glfw.org/download.html)
 - [GLM](https://github.com/g-truc/glm)
 - [tiny_obj_loader header file](https://github.com/tinyobjloader/tinyobjloader/blob/release/tiny_obj_loader.h)
 
 **Note: If you use linux**, probably, you will need the [GLSL Compiler (glslc)](https://github.com/google/shaderc) (usually inclued on Vulkan SDK, but I need to added manually). You could compile it (or download the binaries) and install it on a custom PATH or add it to the standard path (inside /usr) 

You could use the [Vulkan tutorial](https://vulkan-tutorial.com/Development_environment#page_GLFW) guide to learn how to install all.
  
You need to use [CMake](https://cmake.org/download/) to build the project (Only on MacOS you can use the xcode project).
  
### Set Vulkan Path, GLFW, GLM, tiny_obj_loader
Also you have to set the Vulkan SDK, GLFW, GLM, tiny_obj_loader path

If you use the xcode project on MacOs follow this youtube video: [Setup Vulkan](https://www.youtube.com/watch?v=_j3ugZwT3Vc).

In my setup I use the standard header an library path to add GLFW, GLM so you will see /usr/local/lib as path.

On CMakeList.txt you should modified it to  set the paths.
#### Windows Example
``` cmake
set(GLM_PATH D:/glm)
set(VULKAN_SDK_PATH  C:/VulkanSDK/1.2.198.1)

#If you want to use a directory for the GLFW library use only set GLFW_PATH
set(GLFW_PATH D:/glfw-3.3.8.bin.WIN64)
#If you want to use diferent directory for headers and lib for the GLFW_PATH only use the next ones
#set(GLFW_INCLUDE_DIRS /usr/local/include)
#set(GLFW_LIB /usr/local/lib)

#MacOs option to avoid code signature validation
#set(NOCODE_SIGN true)

#Optional if you need to specify
#set(GLFW_DLL -lglfw.3)
#set(GLM_DLL -lglm_shared)

# Set MINGW_PATH if using mingwBuild.bat and not VisualStudio20XX
set(MINGW_PATH D:/Qt/Tools/mingw900_64)

# Optional set TINYOBJ_PATH to target specific version, otherwise defaults to external/tinyobjloader
set(TINYOBJ_PATH D:/tinyObj)
```
**Note:** To fix the error message *The CXX compiler identification is unknown* I use the cmake_gui and click con configure and generate.

### MacOS Example
``` cmake
# set(GLM_PATH X:/dev/Libraries/glm)
# set(VULKAN_SDK_PATH  X:/VulkanSDK/1.2.182.0)

#If you want to use a directory for the GLFW library use only set GLFW_PATH
# set(GLFW_PATH X:/dev/Libraries/glfw-3.3.4.bin.WIN64)
#If you want to use diferent directory for headers and lib for the GLFW_PATH only use the next ones
set(GLFW_INCLUDE_DIRS /usr/local/include)
set(GLFW_LIB /usr/local/lib)

#MacOs option to avoid code signature validation
set(NOCODE_SIGN true)

#Optional if you need to specify
set(GLFW_DLL -lglfw.3)
set(GLM_DLL -lglm_shared)

# Set MINGW_PATH if using mingwBuild.bat and not VisualStudio20XX
# set(MINGW_PATH "C:/Program Files/mingw-w64/x86_64-8.1.0-posix-seh-rt_v6-rev0/mingw64")

# Optional set TINYOBJ_PATH to target specific version, otherwise defaults to external/tinyobjloader
set(TINYOBJ_PATH /Users/whitephoenix/tinyObj)
```
**Note:** If I use the a non official terminal (like the terminal inside vscode) I got an error message when I tried to run the App. I should use the terminal app native of MacOs.

### Linux Example
``` cmake
# set(GLM_PATH X:/dev/Libraries/glm)
# set(VULKAN_SDK_PATH  X:/VulkanSDK/1.2.182.0)

#If you want to use a directory for the GLFW library use only set GLFW_PATH
# set(GLFW_PATH X:/dev/Libraries/glfw-3.3.4.bin.WIN64)
#If you want to use diferent directory for headers and lib for the GLFW_PATH only use the next ones
#set(GLFW_INCLUDE_DIRS /usr/local/include)
#set(GLFW_LIB /usr/local/lib)

#MacOs option to avoid code signature validation
#set(NOCODE_SIGN true)

#Optional if you need to specify
#set(GLFW_DLL -lglfw.3)
#set(GLM_DLL -lglm_shared)

# Set MINGW_PATH if using mingwBuild.bat and not VisualStudio20XX
# set(MINGW_PATH "C:/Program Files/mingw-w64/x86_64-8.1.0-posix-seh-rt_v6-rev0/mingw64")

# Optional set TINYOBJ_PATH to target specific version, otherwise defaults to external/tinyobjloader
set(TINYOBJ_PATH /home/avisphoenix/tinyObj)
```
## Compilation
To compile the project you should execute the created script:
- unixBuild.sh (Linux, MacOS)
- mingwBuild.sh (Windows)

**Note:** If you are using the xcode project you just need to configure the paths and run it inside the XCode IDE.
## License
GNU GPL v3 [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Project status

Currrently until tutorial 27

We do:
- Add Multi Sample Anti Alias Support (MSAA)
- Use AlphaBlending on the ligths
- Using Phong shaders (simple_shader)
- Improve CMakeList.txt to let us refactoring the folder structure on code
- Draw 2D points (as light)
- Cross-platform system (Tested on Linux, Windows, MacOS)
- Use CMake to build the project (Cross-platform)
- Add shaders compilation to the xcode project phase
- Improve folder scheme of files
- Fix dependecy of the variable path (used absolute paths, now use relative paths)
- Add fragment lighting shader
- Add point light
- Add Uniform Buffer
- Diffuse ligth shader
- Load OBJ files
- Add Index Buffer
- Add Keyboard Input to move camera (GLFW)
- Fix fps dependency on animations using deltatime (or differential time)
- Add basic camera (position and target)
- ~~Draw  and rotate~~ ~~Triangle~~ ~~Cube~~ (before loading objs)
- Set window initial size with resizable mode
- Use GLFW
- Use XCode project
- Fix previuos vulkan validation error message (on close windows)

To Do:
- Refactor and improve Single Responsability
- Finish tutorial

## Complementary Documentation

- [Game Programming Patterns: GameLoop](https://gameprogrammingpatterns.com/game-loop.html)
- [TimeStep](https://gafferongames.com/post/fix_your_timestep/)
- [Vulkan Guide: Abstraction of descriptors](https://vkguide.dev/docs/extra-chapter/abstracting_descriptors/)
- [Entity Component System](https://austinmorlan.com/posts/entity_component_system/)
- [Billboards](https://flipcode.com/archives/Billboarding-Excerpt_From_iReal-Time_Renderingi_2E.shtml) (2D inside 3D world)


### Compile the shaders (Optional)
If you need to compile all the shaders:
~~~
glslc simple_shader.frag -o simple_shader.frag.spv
glslc simple_shader.vert -o simple_shader.vert.spv
~~~

### Update Paths (Optional)
Usually you don't need to change the paths, but if you need to changes the default paths of models and shaders you could do it following the next steps:

1. Go to HelloVulkan/src/HelloVulkanLib/HelloVulkanDefaultPaths.h
1. Look for SHADERS_DIR and MODELS_DIR variable Path and change the path for your own. (this change cannot be uploaded).
